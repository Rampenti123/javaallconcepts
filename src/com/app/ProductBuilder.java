package com.app;

public class ProductBuilder {

	private String brandName;
	private int cost;
	private int quantity;
	private String Shop;
	public String getBrandName() {
		return brandName;
	}
	public ProductBuilder setBrandName(String brandName) {
		this.brandName = brandName;
		return this;
	}
	
	public ProductBuilder setCost(int cost) {
		this.cost = cost;
		return this;
	}
	
	public ProductBuilder setQuantity(int quantity) {
		this.quantity = quantity;
		return this;
	}
	
	public ProductBuilder setShop(String shop) {
		Shop = shop;
		return this;
	}
	public Rice getRice() {
		return new Rice(brandName, cost, quantity, Shop);

	}
	
}
