package com.app.stringexamples;

import java.util.Locale;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StringExampleMethods {

	public static void main(String[] args) {
		String string = "Helou";
		string.charAt(0);
		IntStream intStream = string.chars();
		System.out.println("codePointAt :" + string.codePointAt(0));
		string.codePointAt(0);
		System.out.println((int) 'H');
		System.out.println("codePointBefore :" + string.codePointBefore(1));
		System.out.println("codePointCount :" + string.codePointCount(2, 5));
		String string1 = "Helou1H";
		// contains method will check the case also
		System.out.println(string.contains(string1));
		System.out.println("translateEscapes :" + string.translateEscapes());
		System.out.println("concat :" + string.concat(string1));
		StringBuffer buffer = new StringBuffer();
		buffer.append('H');
		buffer.append('e');
		buffer.append('l');
		buffer.append('o');
		buffer.append('u');
		System.out.println("contentEquals :" + string.contentEquals(buffer));
		System.out.println("describeConstable :" + "WW".describeConstable());
		System.out.println("endsWith :" + string.endsWith("u"));
		System.out.println("equals :" + string.equals("Helou"));
		System.out.println("equalsIgnoreCase : " + string.equalsIgnoreCase("helou"));
		String[] stringValues = { "u", "Ramu", "Naveen", "Amma" };
		System.out.println("formatted : " + string.formatted(stringValues));
		System.out.println("hashCode : " + string1.hashCode());
		System.out.println("indent : " + "3".indent(3));
		System.out.println("indexOf : " + string.indexOf(0));
		System.out.println("indexOf : " + string.indexOf("H", 2));
		/**
		 * @param String what letter to be find int search starting position
		 */
		System.out.println("indexOf : " + "iysys" + "HaHak".indexOf("k", 0));		
		System.out.println("formatted : " +string.hashCode());
		System.out.println("formatted : " +string.hashCode());
		System.out.println("formatted : " +string.hashCode());
		System.out.println("intern : " + string.intern());
		System.err.println("isBlank : " + string.isBlank());
		System.err.println("isEmpty : " + string.isEmpty());
		System.out.println("matches : " + string.matches("Helou"));
		System.out.println("offsetByCodePoints : " + string.offsetByCodePoints(1,1));
//		System.out.println(string.regionMatches(1, "Hello", 2,1));
		System.out.println(string.repeat(3));
		System.out.println(string.replace('k','a'));
		System.out.println("akjakha kaaka akaka".replaceAll(" ","2"));
		System.out.println("akjakha kaaka akaka".replaceFirst(" ","2"));
		System.out.println("ahhaja".resolveConstantDesc(null));
		System.out.println("startsWith : " +string.startsWith("H"));
		System.out.println("startsWith : " +"ajahjaajhja jhaja ".startsWith("j", 1));
		System.out.println("strip : " +"ajahjaajhja jhaja ".strip());
		System.out.println("stricopyValueOfpIndent : " +"ajahjaajhja jhaja 2".stripIndent());
		System.out.println("stripLeading : " +"ajahjaajhja jhaja 2".stripLeading());
		System.out.println("copyValueOf : " +"aj2hjaajhja jhaja 2".substring(2));
		System.out.println("copyValueOf : " +"aj2hjaajhja jhaja 2".substring(2,4));
		System.out.println("copyValueOf : " +"aj2hjaajhja jhaja 2".subSequence(2,8));
		System.out.println(string.toCharArray());
		char[] shhs= {'a','w','w','w','f'};
		System.out.println("copyValueOf : " + string.copyValueOf(shhs));
		System.out.println("toLowerCase : " + string.toLowerCase(Locale.SIMPLIFIED_CHINESE));
		System.out.println("translateEscapes : " + string.translateEscapes());
		System.out.println("copyValueOf" + string.copyValueOf(shhs,1,4));
		System.out.println("format" + string.format(Locale.CANADA," ",4));
		System.out.println("valueOf" + string.valueOf(true));
		System.out.println("valueOf" + string.valueOf(10));

		

	}

}
