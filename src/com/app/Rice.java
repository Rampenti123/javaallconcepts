package com.app;

public class Rice {

	private String brandName;
	private int cost;
	private int quantity;
	private String Shop;

	public Rice(String brandName, int cost, int quantity, String shop) {
		super();
		this.brandName = brandName;
		this.cost = cost;
		this.quantity = quantity;
		this.Shop = shop;
	}

	@Override
	public String toString() {
		return "Rice [brandName=" + brandName + ", cost=" + cost + ", quantity=" + quantity + ", Shop=" + Shop + "]";
	}
	

}
