package com.app;

/**
 * Build Factory in Creational Pattern
*/
public class Product {

	public static void main(String[] args) {
		Rice rice=new ProductBuilder().setBrandName("RedMallelu").getRice();
		System.out.println(rice);
		System.out.println(rice);
	}
}
